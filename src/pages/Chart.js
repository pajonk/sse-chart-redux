import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getCurrentChart } from '../reducers'
import * as charts from '../constants/ChartTypes';
import FilterLink from '../containers/FilterLink';
import LiveChart from '../containers/LiveChart';
import StaticChart from "../containers/StaticChart";

const ChartPage = ({currentChart}) => {
  const chartPart = currentChart === charts.CHART_REAL_TIME ?
    <LiveChart /> : <StaticChart/>;

  const filtersPart = (
    <div className="Selection-filters">
      <ul className="List List--inline">
        <li className="List-item--clean">Show:</li>
        <li>
          <FilterLink type={charts.CHART_STATIC}>
            Last 30 days
          </FilterLink>
        </li>
        <li>
          <FilterLink type={charts.CHART_REAL_TIME}>
            Real time data
          </FilterLink>
        </li>
      </ul>
    </div>
  );

  return (
    <div className="Wrapper">
      <p><NavLink to="/">&larr; Go back to Selections List</NavLink></p>
      <main className="Selection">
        <h1 className="Selection-title">Sample selection</h1>
        <section className="Selection-desc">
          <h3>Description</h3>
          <p>Unique customers that bought tomatoes within last month</p>
        </section>
        <section className="Selection-charts">
          <h3>Selection activity</h3>
          { filtersPart }
          { chartPart }
        </section>
      </main>
    </div>
  )
};

const mapStateToProps = (
  state,
  ownProps
) => ({
  currentChart: getCurrentChart(state)
});

export default connect(
  mapStateToProps
)(ChartPage);