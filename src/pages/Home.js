import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

const HomePage = () => (
  <div className="Wrapper">
    <header>
      <h1>Selections</h1>
      <p>Selections are rules to select visitor Profiles based on their actions (Engagements) and other data (External Facts).<br />
        All active selections and their most recent activity are displayed below.</p>
    </header>
    <ul>
      <li>
        <NavLink to="/chart">Sample Selection</NavLink>
      </li>
    </ul>
  </div>
);

export default connect()(HomePage);