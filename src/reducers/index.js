import { combineReducers } from 'redux';
import chart from './chart';
import * as fromChart from './chart';

const app = combineReducers({
  chart
});

export default app;

export const getCurrentChart = (state) => {
  return fromChart.getCurrentChart(state.chart);
};

export const getLastPoint = (state) => {
  return fromChart.getLastPoint(state.chart);
};