import { combineReducers } from 'redux';
import * as types from '../constants/ActionTypes';
import * as charts  from '../constants/ChartTypes';

/**
 * Data Points
 * @param state
 * @param action
 * @returns {state}
 */
const byId = (state = {}, action) => {
  switch (action.type) {
    case types.CHART_DATA_RECEIVED:
      return {
        ...state,
        [action.data.key.segmentNumber]: action.data
      };
    default:
      return state;
  }
};

/**
 * All IDs of Data Points
 * @param state
 * @param action
 * @returns [state]
 */
const allIds = (state = [], action) => {
  switch (action.type) {
    case types.CHART_DATA_RECEIVED:
      return [
        ...state,
        action.data.key.segmentNumber
      ];
    default:
      return state;
  }
};

const uiInit = {
  chartType: charts.CHART_STATIC
};

/**
 * UI Reducer
 * @param state
 * @param action
 * @returns {{chartType}}
 */
const ui = (state = uiInit, action) => {
  switch (action.type) {
    case types.CHART_SET_TYPE:
      return {
        ...state,
        chartType: action.payload
      };
    default:
      return state;
  }
};

export default combineReducers({
  byId,
  allIds,
  ui
});

/**
 * Get last data point for real time chart
 * @param state
 * @returns {*}
 */
export const getLastPoint = (state) => {
  return state.byId[state.allIds[state.allIds.length - 1]];
};

/**
 * Get current type of displaying chart
 * @param state
 * @returns {*}
 */
export const getCurrentChart = (state) => {
  return state.ui.chartType;
};