import reducer from './chart';
import * as selectors from './index';
import * as types from '../constants/ActionTypes';
import * as charts  from '../constants/ChartTypes';

const uiInit = {
  chartType: charts.CHART_STATIC
};

const defaultState = {
  byId: {},
  allIds: [],
  ui: uiInit
};

const serverData = {
  "key": {
    "segmentNumber": 188,
    "dayTimestamp": 1496181600000,
    "timestamp": 1496181600000
  },
  "totalCallsAdded": 31,
  "totalCallsRemoved": 28,
  "segmentSize": 17
};

describe('chart reducer', () => {

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(defaultState);
  });

  it('should handle CHART_SET_TYPE', () => {
    const expectedState = Object.assign({}, defaultState, {
      ui: Object.assign({}, uiInit, {
        chartType: charts.CHART_REAL_TIME
      })
    });
    expect(
      reducer(undefined, {
        type: types.CHART_SET_TYPE,
        payload: charts.CHART_REAL_TIME
      })
    ).toEqual(expectedState)
  });

  it('should handle CHART_DATA_RECEIVED', () => {
    const expectedState = Object.assign({}, defaultState, {
      allIds: [188],
      byId: {
        188: serverData
      }
    });
    expect(
      reducer(undefined, {
        type: types.CHART_DATA_RECEIVED,
        data: serverData
      })
    ).toEqual(expectedState)
  });

});

describe('chart selectors', () => {

  it('should return current chart type', () => {
    expect(selectors.getCurrentChart({chart: defaultState})).toEqual(
      charts.CHART_STATIC
    )
  });

  it('should return last data point', () => {
    const currentState = {
      chart: {
        allIds: [121, 188],
        byId: {
          188: serverData,
          121: {id: "121"}
        }
      }
    };
    expect(selectors.getLastPoint(currentState)).toEqual(serverData)
  });


});