import * as types from '../constants/ActionTypes';
import { SERVER_EMITTER_URL } from '../constants/AppConstants'

/**
 * Set current chart type
 * @param payload
 * @returns {{type, payload: *}}
 */
export const setChart = (payload) => {
  return {
    type: types.CHART_SET_TYPE,
    payload
  }
};

/**
 * Subscribe to EventSource with real time chart data
 * @returns EventSource
 */
export const subscribeToStream = () => (dispatch) => {
  // Connect to Event Source
  const es = new EventSource(SERVER_EMITTER_URL);

  // Subscribe to real time data
  es.onmessage = function(message) {
    const data = message && message.data && JSON.parse(message.data);
    dispatch({
      type: types.CHART_DATA_RECEIVED,
      data
    })
  };

  // Return Event Source
  // to close connection when needed
  return es;
};

