import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as types from '../constants/ActionTypes';
import * as charts from '../constants/ChartTypes';
import * as actions from './chart';
import { SERVER_EMITTER_URL } from '../constants/AppConstants'

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore({ chart: {ui: {}, byId: {}, allIds: []} });

describe('chart', () => {

  it('should create an action to set chart type', () => {
    const payload = charts.CHART_REAL_TIME;
    const expectedAction = {
      type: types.CHART_SET_TYPE,
      payload
    };
    expect(actions.setChart(payload)).toEqual(expectedAction)
  });

  it('should subscribe to real time data stream and dispatch events', () => {
    global.EventSource = jest.fn().mockImplementation(() => ({close: () => "closed"}));

    const listener = store.dispatch(actions.subscribeToStream());
    const data1 = "{\"id\": \"dataId1\"}";
    const data2 = "{\"id\": \"dataId2\"}";
    const expectedActions = [
      { type: types.CHART_DATA_RECEIVED, data: {id: "dataId1"}},
      { type: types.CHART_DATA_RECEIVED, data: {id: "dataId2"}}
    ];

    listener.onmessage({data: data1});
    listener.onmessage({data: data2});

    expect(global.EventSource).toHaveBeenCalledWith(SERVER_EMITTER_URL);
    expect(store.getActions()).toEqual(expectedActions);
    expect(typeof listener.close).toEqual('function');
  });

});