import React, { Component } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

import HomePage from '../../pages/Home';
import ChartPage from '../../pages/Chart';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Route scrollStrategy exact path="/" component={HomePage}/>
          <Route scrollStrategy exact path="/chart" component={ChartPage}/>
        </div>
      </Router>
    );
  }
}

export default App;
