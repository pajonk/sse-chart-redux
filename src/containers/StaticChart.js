import React from 'react';
import ReactHighcharts from 'react-highcharts';
import { getDefaultConfig, convertToSeries, historicalData } from '../helpers/chartHelpers';

const StaticChart = () => {
  const config = getDefaultConfig();
  config.series = convertToSeries(historicalData);
  return (
    <div className="Chart">
      <ReactHighcharts config={config} />
    </div>
  );
};

export default StaticChart;