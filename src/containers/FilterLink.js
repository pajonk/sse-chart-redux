import React from 'react';
import {connect} from 'react-redux';
import { setChart } from '../actions/chart';
import * as selectors from '../reducers/index'
import ButtonLink from '../components/ButtonLink/index';

const FilterLink = (props) => (
  <ButtonLink {...props}>{props.children}</ButtonLink>
);

const mapStateToProps = (
  state,
  ownProps
) => ({
  type: ownProps.type,
  isActive: ownProps.type === selectors.getCurrentChart(state)
});

const mapDispatchToProps = (dispatch) => ({
  onClick: (type) => {
    dispatch(setChart(type))
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilterLink);