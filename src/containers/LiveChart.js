import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactHighcharts from 'react-highcharts';
import { subscribeToStream } from '../actions/chart';
import { getLastPoint } from '../reducers';
import { getDefaultConfig, generateEmptyLabels, convertToSeries } from '../helpers/chartHelpers';

class LiveChart extends Component {
  constructor (props) {
    super(props);
    this.currentSeries = null;
  };

  componentWillMount () {
    const { store } = this.context;
    this.eventsStreamSubscriber = this.props.onInit();
    this.unsubscribeStore  = store.subscribe(() => {
      const data = getLastPoint(store.getState());

      if (!this.currentSeries) return;
      this.drawPoint(0, data, data.totalCallsAdded);
      this.drawPoint(1, data, data.totalCallsRemoved);
      this.drawPoint(2, data, data.segmentSize);
    });
  }

  drawPoint (index, data, y) {
    this.currentSeries[index].addPoint([data.key.timestamp,y], true, true);
  }

  componentWillUnmount () {
    const { unsubscribeStore, eventsStreamSubscriber } = this;
    if (unsubscribeStore && typeof unsubscribeStore === 'function') {
      unsubscribeStore();
    }
    if (eventsStreamSubscriber && typeof eventsStreamSubscriber.close === 'function') {
      eventsStreamSubscriber.close();
    }
  }

  render() {
    // Set current series on chart load
    const registerSeries = (series) => {
      this.currentSeries = series;
    };

    // Prepare data for chart
    const emptyLabels = generateEmptyLabels(20);
    const config = getDefaultConfig();
    config.series = convertToSeries({
      added: [...emptyLabels],
      removed: [...emptyLabels],
      segmentSize: [...emptyLabels],
    });
    config.chart.events.load = function () {
      registerSeries(this.series);
    };

    return (
      <div className="Chart">
        <ReactHighcharts config={config} />
      </div>
    );
  }
}
LiveChart.contextTypes = {
  store: React.PropTypes.object
};

const mapDispatchToProps = (dispatch) => ({
  onInit: () => dispatch(subscribeToStream())
});

export default connect(
  null,
  mapDispatchToProps
)(LiveChart);