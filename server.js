const SSE = require("sse-node");
const cors = require('cors');
const app = require("express")();

function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomMax(max) {
  return getRandom(0, max);
}

let internalId = 0;
let lastSize = 21;
const generatePayload = () => {
  const timestamp = (new Date()).getTime() + 1;
  const timestampToMidnight = (new Date()).setUTCHours(0,0,0,0);
  const added = getRandomMax(50);
  const removed = getRandom(lastSize*-1, 0);
  lastSize = lastSize + added + removed;
  return {
    "key": {
      "segmentNumber": internalId++, // internal segment id
      "dayTimestamp": timestampToMidnight, // timestamp normalised to midnight
      "timestamp": timestamp // actual data-point timestamp
    },
    "totalCallsAdded": added,
    "totalCallsRemoved": removed,
    "segmentSize": lastSize
  };
};

app.use(cors());
app.get("/sse", (req, res) => {
  const client = SSE(req, res);
  console.log("Client connected");

  let timer;
  let sendEvent = () => {
    client.send(generatePayload());
    timer = setTimeout(sendEvent, getRandom(1000,8000));
  };
  timer = setTimeout(sendEvent, 1000);

  client.onClose(() => {
    clearTimeout(timer);
    console.log("Client disconnected");
  });
});

app.listen(8210);